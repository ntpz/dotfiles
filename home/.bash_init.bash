#!/usr/bin/env bash

# bash history settings
# /etc/profile.d/best_bash_history.sh
# Save 5,000 lines of history in memory
HISTSIZE=10000
# Save 2,000,000 lines of history to disk (will have to grep ~/.bash_history for full listing)
HISTFILESIZE=2000000
# Append to history instead of overwrite
shopt -s histappend
# Ignore redundant or space commands
HISTCONTROL=ignoreboth
# Ignore more
HISTIGNORE='ls:ll:ls -alh:pwd:clear:history'
# Set time format
HISTTIMEFORMAT='%F %T '
# Multiple commands on one line show up as a single line
shopt -s cmdhist
# Append new history lines, clear the history list, re-read the history list, print prompt.
export PROMPT_COMMAND="history -a; history -c; history -r; $PROMPT_COMMAND"


# homeshick
if [ -f "$HOME/.homesick/repos/homeshick/homeshick.sh" ] ; then
    source "$HOME/.homesick/repos/homeshick/homeshick.sh"
    source "$HOME/.homesick/repos/homeshick/completions/homeshick-completion.bash"
    homeshick --quiet refresh
fi


# starship
if hash starship 2>/dev/null; then
   eval "$(starship init bash)"
fi


# pyenv
if [ -d "$HOME/.pyenv/bin" ] ; then
    export PATH="$HOME/.pyenv/bin:$PATH"
    eval "$(pyenv init -)"
fi

# pyenv-virtualenv
if [ -d "$HOME/.pyenv/plugins/pyenv-virtualenv" ] ; then
    eval "$(pyenv virtualenv-init -)"
fi

# pyenv-virtualenvwrapper
if [ -d "$HOME/.pyenv/plugins/pyenv-virtualenvwrapper" ] ; then
    eval "$(pyenv virtualenvwrapper_lazy)"
fi

# virtualenvwrapper
if [ -f "/usr/local/bin/virtualenvwrapper.sh" ] ; then
    export PROJECT_HOME="$HOME/Projects"
    export VIRTUALENVWRAPPER_SCRIPT=/usr/local/bin/virtualenvwrapper.sh
    export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python
    source /usr/local/bin/virtualenvwrapper_lazy.sh
fi


# fly.io
if [ -d "$HOME/.fly" ] ; then
    export FLYCTL_INSTALL="$HOME/.fly"
    export PATH="$FLYCTL_INSTALL/bin:$PATH"
fi


# fnm
if [ -d "$HOME/.fnm" ] ; then
    export PATH="$HOME/.fnm:$PATH"
    eval "$(fnm env --use-on-cd)"
    eval "$(fnm completions)"
fi

# fnm, newer location
if [ -d "$HOME/.local/share/fnm" ] ; then
    export PATH="$HOME/.local/share/fnm:$PATH"
    eval "`fnm env`"
fi


# sdkman
if [ -d "$HOME/.sdkman" ] ; then
    export SDKMAN_DIR="$HOME/.sdkman"
    [[ -s "$HOME/.sdkman/bin/sdkman-init.sh" ]] && source "$HOME/.sdkman/bin/sdkman-init.sh"

fi
